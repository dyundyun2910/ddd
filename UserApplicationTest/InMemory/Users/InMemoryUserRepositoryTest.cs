﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Net.Mail;
using UsersApplication.Domain.Models.Users;
using UsersApplication.InMemory.Users;

namespace UserApplicationTest.InMemory.Users
{
    [TestClass]
    public class InMemoryUserRepositoryTest
    {
        private IUserFactory userFactory;
        private InMemoryUserRepository userRepository;

        [TestInitialize]
        public void SetUp()
        {
            userFactory = new InMemoryUserFactory();
            userRepository = new InMemoryUserRepository();
        }

        [TestMethod]
        public void TestSave()
        {

            var user = userFactory.Create(new UserName("takaki"));
            userRepository.Save(user);

            Assert.AreEqual(user.Name.Value, userRepository.Store.Values.First().Name.Value);
        }

        [TestMethod]
        public void TestFindUserByName()
        {
            var userName = new UserName("takaki");
            var user = userFactory.Create(userName);
            userRepository.Save(user);

            Assert.AreEqual(userName.Value, userRepository.Find(userName).Name.Value);
        }

        [TestMethod]
        public void TestNotFindByNameWithNoElement()
        {
            Assert.IsNull(userRepository.Find(new UserName("NothingElements")));
        }

        [TestMethod]
        public void TestNotFindByNameWithOneElement()
        {
            var userName = new UserName("takaki");
            var user = userFactory.Create(userName);
            userRepository.Save(user);

            Assert.IsNull(userRepository.Find(new UserName("NotSavedName")));
        }

        [TestMethod]
        public void TestFindUserById()
        {
            var userName = new UserName("takaki");
            var userId = new UserId("someId");
            var mail = new MailAddress("some@example.com");
            var user = new User(userId, userName, mail);
            userRepository.Save(user);

            Assert.AreEqual(userId, userRepository.Find(userId).Id);
        }

        [TestMethod]
        public void TestNotFindByIdWithNoElement()
        {
            Assert.IsNull(userRepository.Find(new UserId("NothingElements")));
        }

        [TestMethod]
        public void TestNotFindByIdWithOneElement()
        {
            var userName = new UserName("takaki");
            var user = userFactory.Create(userName);
            userRepository.Save(user);

            Assert.IsNull(userRepository.Find(new UserId("NotSavedName")));
        }

        [TestMethod]
        public void TestDeleteUser()
        {
            var userName = new UserName("takaki");
            var user = userFactory.Create(userName);
            userRepository.Save(user);

            Assert.AreEqual(user.Id, userRepository.Find(user.Id).Id);

            userRepository.Delete(user);

            Assert.IsNull(userRepository.Find(user.Id));
        }

        [TestMethod]
        public void TestDeleteNotExistUserNotThrowException()
        {
            var userName = new UserName("takaki");
            var user = userFactory.Create(userName);
            userRepository.Delete(user);
        }

    }
}
