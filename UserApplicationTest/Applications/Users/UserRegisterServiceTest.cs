﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UsersApplication.Applications.Users;
using UsersApplication.Domain.Models.Users;
using UsersApplication.Domain.Services.Users;
using UsersApplication.InMemory.Users;

namespace UserApplicationTest.Applications.Users
{
    [TestClass]

    public class UserRegisterServiceTest
    {
        private IUserRegisterService userRegisterService;
        private IUserRepository userRepository;

        [TestInitialize]
        public void Setup()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IUserFactory, InMemoryUserFactory>();
            serviceCollection.AddSingleton<IUserRepository, InMemoryUserRepository>();
            serviceCollection.AddTransient<UserService>();
            serviceCollection.AddTransient<UserRegisterService>();

            var provider = serviceCollection.BuildServiceProvider();
            userRegisterService = provider.GetService<UserRegisterService>();
            userRepository = provider.GetService<IUserRepository>();
        }

        [TestCleanup]
        public void TearDown() { }

        [TestMethod]
        public void TestRegisterUser()
        {
            var userName = "takaki";
            var command = new UserRegisterCommand(userName);
            userRegisterService.Handle(command);

            var createdUserName = new UserName(userName);
            var createdUser = userRepository.Find(createdUserName);
            Assert.IsNotNull(createdUser);
        }

        [TestMethod]
        public void TestSuccessMinUserName()
        {
            var minUserName = "123";
            var command = new UserRegisterCommand(minUserName);
            userRegisterService.Handle(command);

            var createdUserName = new UserName(minUserName);
            var createdUser = userRepository.Find(createdUserName);
            Assert.IsNotNull(createdUser);
        }

        [TestMethod]
        public void TestSuccessMaxUserName()
        {
            var maxUserName = "1234567890123456789";
            var command = new UserRegisterCommand(maxUserName);
            userRegisterService.Handle(command);

            var createdUserName = new UserName(maxUserName);
            var createdUser = userRepository.Find(createdUserName);
            Assert.IsNotNull(createdUser);
        }

        [TestMethod]
        public void TestInvalidUserNameLengthMin()
        {
            var minUserName = "12";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var command = new UserRegisterCommand(minUserName);
                userRegisterService.Handle(command);
            }
            );
            Assert.AreEqual("ユーザ名は３文字以上です\r\nパラメーター名:value", ex.Message);
        }

        [TestMethod]
        public void TestInvalidUserNameLengthMax()
        {
            var minUserName = "123456789012345678901";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var command = new UserRegisterCommand(minUserName);
                userRegisterService.Handle(command);
            }
            );
            Assert.AreEqual("ユーザ名は２０文字以下です\r\nパラメーター名:value", ex.Message);
        }

        [TestMethod]
        public void TestAlreadyExites()
        {
            var command = new UserRegisterCommand("takaki");
            userRegisterService.Handle(command);
            var ex = Assert.ThrowsException<CanNotRegisterUserException>(() =>
            {
                userRegisterService.Handle(command);
            }
             );
            Assert.AreEqual("種類 'UsersApplication.Applications.Users.CanNotRegisterUserException' の例外がスローされました。", ex.Message);
        }
    }
}
