﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UsersApplication.Domain.Models.Users;

namespace UserApplicationTest.Domain.Users
{
    [TestClass]
    public class UserNameTest
    {
        [TestMethod]
        public void TestCreateUserName()
        {
            var name = "Takaki jun";
            var userName = new UserName(name);
            Assert.AreEqual(name, userName.Value);
        }

        [TestMethod]
        public void TestUserNameMustBeLongerThan3()
        {
            var name = "sa";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var userName = new UserName(name);
            }
             );
            Assert.AreEqual("ユーザ名は３文字以上です\r\nパラメーター名:value", ex.Message);
        }

        public void TestUserNameMustBeShorterThan20_ASCII()
        {
            var name = "123456789012345678901";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var userName = new UserName(name);
            }
             );
            Assert.AreEqual("ユーザ名は２０文字以下です\r\nパラメーター名:value", ex.Message);
        }

        public void TestUserNameMustBeShorterThan20_MultiByte()
        {
            var name = "あいうえおかきくけこさしすせそたちつてと漢字";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var userName = new UserName(name);
            }
             );
            Assert.AreEqual("ユーザ名は２０文字以下です\r\nパラメーター名:value", ex.Message);
        }

        public void TestUserNameMustBeShorterThan20_SurrogatePair()
        {
            var name = "あいうえおかきくけこさしすせそたちつてと𩸽";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var userName = new UserName(name);
            }
             );
            Assert.AreEqual("ユーザ名は２０文字以下です\r\nパラメーター名:value", ex.Message);
        }

    }
}
