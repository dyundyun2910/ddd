﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UsersApplication.Domain.Models.Users;

namespace UserApplicationTest.Domain.Users
{
    [TestClass]
    public class UserIdTest
    {
        [TestMethod]
        public void TestThrowExceptionWhenCreateUserIdWithNull()
        {
            Assert.ThrowsException<ArgumentException>(() => { new UserId(null); });
        }

        [TestMethod]
        public void TestThrowExceptionWhenCreateUserIdWithEmptyString()
        {
            Assert.ThrowsException<ArgumentException>(() => { new UserId(string.Empty); });
        }

        [TestMethod]
        public void TestCreateUserId()
        {
            var id = Guid.NewGuid().ToString();
            var userId = new UserId(id);
            Assert.AreEqual(id, userId.Value);
        }
    }
}
