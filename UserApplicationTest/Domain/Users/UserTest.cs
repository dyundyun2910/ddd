﻿using System;
using System.Net.Mail;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UsersApplication.Domain.Models.Users;
using UsersApplication.InMemory.Users;

namespace UserApplicationTest.Domain.Users
{
    [TestClass]
    public class UserTest
    {
        private IUserFactory userFactory = new InMemoryUserFactory();

        [TestMethod]
        public void TestThrowExceptionWhenCreateUserWithNull()
        {
            Assert.ThrowsException<ArgumentNullException>(() => { userFactory.Create(null); });
        }

        [TestMethod]
        public void TestCreateUser()
        {
            var userName = new UserName("Takaki jun");
            var user = userFactory.Create(userName);

            Assert.AreEqual(userName, user.Name);
        }

        [TestMethod]
        public void TestChangeName()
        {
            var userName = new UserName("Takaki jun");
            var user = userFactory.Create(userName);

            var newName = new UserName("NewName");
            user.ChangeName(newName);

            Assert.AreEqual(newName, user.Name);
        }

        [TestMethod]
        public void TestChangeMailAddress()
        {
            var userName = new UserName("Takaki jun");
            var user = userFactory.Create(userName);

            Assert.AreEqual(null, user.Mail);

            var mail = new MailAddress("some@example.com");
            user.ChangeMailAddress(mail);

            Assert.AreEqual(mail, user.Mail);
        }
    }
}
