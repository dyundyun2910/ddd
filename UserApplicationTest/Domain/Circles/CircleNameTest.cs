﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UsersApplication.Domain.Models.Circles;

namespace UserApplicationTest.Domain.Circles
{
    [TestClass]
    public class CircleNameTest
    {
        [TestMethod]
        public void TestCreateCircleName()
        {
            var name = "Tennis";
            var circleName = new CircleName(name);
            Assert.AreEqual(name, circleName.Value);
        }

        [TestMethod]
        public void TestCircleNameMustBeLongerThan3()
        {
            var name = "sa";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var userName = new CircleName(name);
            }
             );
            Assert.AreEqual("サークル名は3文字以上です。\r\nパラメーター名:value", ex.Message);
        }

        public void TestCircleNameMustBeShorterThan20_ASCII()
        {
            var name = "123456789012345678901";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var circleName = new CircleName(name);
            }
             );
            Assert.AreEqual("サークル名は20文字以下です。\r\nパラメーター名:value", ex.Message);
        }

        public void TestCircleNameMustBeShorterThan20_MultiByte()
        {
            var name = "あいうえおかきくけこさしすせそたちつてと漢字";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var circleName = new CircleName(name);
            }
             );
            Assert.AreEqual("サークル名は20文字以下です。\r\nパラメーター名:value", ex.Message);
        }

        public void TestCircleNameMustBeShorterThan20_SurrogatePair()
        {
            var name = "あいうえおかきくけこさしすせそたちつてと𩸽";
            var ex = Assert.ThrowsException<ArgumentException>(() =>
            {
                var circleName = new CircleName(name);
            }
             );
            Assert.AreEqual("サークル名は20文字以下です。\r\nパラメーター名:value", ex.Message);
        }

    }
}
