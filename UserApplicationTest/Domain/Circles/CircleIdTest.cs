﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UsersApplication.Domain.Models.Circles;

namespace UserApplicationTest.Domain.Circles
{
    [TestClass]
    public class CircleIdTest
    {
        [TestMethod]
        public void TestThrowExceptionWhenCreateCircleIdWithNull()
        {
            Assert.ThrowsException<ArgumentNullException>(() => { new CircleId(null); });
        }

        [TestMethod]
        public void TestThrowExceptionWhenCreateCircleIdWithEmptyString()
        {
            Assert.ThrowsException<ArgumentException>(() => { new CircleId(string.Empty); });
        }

        [TestMethod]
        public void TestCreateCircleId()
        {
            var id = Guid.NewGuid().ToString();
            var userId = new CircleId(id);
            Assert.AreEqual(id, userId.Value);
        }
    }
}
