﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UsersApplication.Domain.Models.Circles;
using UsersApplication.Domain.Models.Users;
using UsersApplication.Applications.Circles;

namespace UserApplicationTest.Domain.Circles
{
    [TestClass]
    public class CircleTest
    {
        [TestMethod]
        public void TestThrowExceptionWhenCreateUCircleWithNull()
        {
            var id = new CircleId(Guid.NewGuid().ToString());
            var name = new CircleName("Tennis");
            var owner = new User(new UserId(Guid.NewGuid().ToString()), new UserName("Takkai"));
            var members = new List<User>(){ owner };

            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, null, null, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, null, null, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, name, null, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, null, owner, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, null, null, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, name, null, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, null, owner, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, null, null, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, name, owner, null); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, name, null, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, null, owner, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(null, name, owner, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, null, owner, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, name, null, members); });
            Assert.ThrowsException<ArgumentNullException>(() => { new Circle(id, name, owner, null); });
        }

        [TestMethod]
        public void TestCreateCircle()
        {
            var id = new CircleId(Guid.NewGuid().ToString());
            var name = new CircleName("Tennis");
            var owner = new User(new UserId(Guid.NewGuid().ToString()), new UserName("Takkai"));
            var members = new List<User>() { owner };

            var circle = new Circle(id, name, owner, members);

            Assert.AreEqual(name, circle.Name);
        }

        [TestMethod]
        public void TestJoin()
        {
            var id = new CircleId(Guid.NewGuid().ToString());
            var name = new CircleName("Tennis");
            var owner = new User(new UserId(Guid.NewGuid().ToString()), new UserName("Takkai"));
            var members = new List<User>() { owner };

            var circle = new Circle(id, name, owner, members);

            var newMemmber = new User(new UserId(Guid.NewGuid().ToString()), new UserName("member"));
            circle.Join(newMemmber);

        }
        [TestMethod]

        public void TestCanNotJoinWhenMemberIsFull()
        {
            var id = new CircleId(Guid.NewGuid().ToString());
            var name = new CircleName("Tennis");
            var owner = new User(new UserId(Guid.NewGuid().ToString()), new UserName("Takkai"));
            var member = new User(new UserId(Guid.NewGuid().ToString()), new UserName("member"));
            var members = new List<User>() { 
                owner, member,member,member,member,member,member,member,member,member,
                member,member,member,member,member,member,member,member,member,member,
                member,member,member,member,member,member,member,member,member,member
            };

            var newMember = new User(new UserId(Guid.NewGuid().ToString()), new UserName("newMember")); 
            var circle = new Circle(id, name, owner, members);

            Assert.ThrowsException<CircleFullException>(() =>
            {
                circle.Join(newMember);
            });
        }
    }
}
