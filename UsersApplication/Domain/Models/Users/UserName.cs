﻿using System;
using System.Collections.Generic;

namespace UsersApplication.Domain.Models.Users
{
    public class UserName : IEquatable<UserName>
    {
        public UserName(string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value.Length < 3) throw new ArgumentException("ユーザ名は３文字以上です", nameof(value));
            if (value.Length > 20) throw new ArgumentException("ユーザ名は２０文字以下です", nameof(value));

            Value = value;
        }

        public bool Equals(UserName other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return string.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            return Equals((UserName)obj);
        }

        public override int GetHashCode()
        {
            return -1937169414 + EqualityComparer<string>.Default.GetHashCode(Value);
        }

        public string Value { get; }
    }
}