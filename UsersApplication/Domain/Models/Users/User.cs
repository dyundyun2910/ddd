﻿using System;
using System.Net.Mail;

namespace UsersApplication.Domain.Models.Users
{
    public class User
    {
        public User(UserId id, UserName name, MailAddress mail = null)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            if (name == null) throw new ArgumentNullException(nameof(name));

            Id = id;
            Name = name;
            Mail = mail;
        }

        public UserId Id { get; }
        public UserName Name { get; private set; }
        public MailAddress Mail { get; private set; }

        public void ChangeName(UserName name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));

            Name = name;
        }

        public void ChangeMailAddress(MailAddress mail)
        {
            Mail = mail;
        }
    }
}
