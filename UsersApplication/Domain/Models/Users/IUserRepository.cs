﻿namespace UsersApplication.Domain.Models.Users
{
    public interface IUserRepository
    {
        void Save(User user);
        User Find(UserName name);
        User Find(UserId id);
        void Delete(User user);
    }
}
