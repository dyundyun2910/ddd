﻿using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Domain.Models.Circles
{
    public interface ICircleFactory
    {
        Circle Create(CircleName name, User owner);
    }
}
