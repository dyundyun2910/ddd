﻿using System;
using System.Collections.Generic;
using UsersApplication.Applications.Circles;
using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Domain.Models.Circles
{
    public class Circle
    {
        private readonly CircleId id;
        private User owner;
        private List<User> members;

        public Circle(CircleId id, CircleName name, User owner, List<User> members)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (owner == null) throw new ArgumentNullException(nameof(owner));
            if (members == null) throw new ArgumentNullException(nameof(members));

            this.id = id;
            this.owner = owner;
            this.members = members;
            Name = name;
        }
        public CircleName Name { get; private set; }

        public void Join(User member)
        {
            if (member == null) throw new ArgumentNullException(nameof(member));

            if (IsFull())
            {
                throw new CircleFullException(id);
            }

            members.Add(member);
        }

        private bool IsFull()
        {
            return members.Count >= 29;
        }
    }
}
