﻿using System;

namespace UsersApplication.Domain.Models.Circles
{
    public class CircleId
    {
        public CircleId(string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value == string.Empty) throw new ArgumentException(nameof(value));

            Value = value;
        }

        public string Value { get; }
    }
}
