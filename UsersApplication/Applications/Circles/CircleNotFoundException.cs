﻿using System;
using System.Runtime.Serialization;
using UsersApplication.Domain.Models.Circles;

namespace UsersApplication.Applications.Circles
{
    [Serializable]
    internal class CircleNotFoundException : Exception
    {
        private CircleId id;
        private string message;

        public CircleNotFoundException()
        {
        }

        public CircleNotFoundException(string message) : base(message)
        {
        }

        public CircleNotFoundException(CircleId id, string message)
        {
            this.id = id;
            this.message = message;
        }

        public CircleNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CircleNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}