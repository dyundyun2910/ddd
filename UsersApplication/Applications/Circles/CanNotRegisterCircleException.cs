﻿using System;
using System.Runtime.Serialization;
using UsersApplication.Domain.Models.Circles;

namespace UsersApplication.Applications.Circles
{
    [Serializable]
    internal class CanNotRegisterCircleException : Exception
    {
        private Circle circle;
        private string message;

        public CanNotRegisterCircleException()
        {
        }

        public CanNotRegisterCircleException(string message) : base(message)
        {
        }

        public CanNotRegisterCircleException(Circle circle, string message)
        {
            this.circle = circle;
            this.message = message;
        }

        public CanNotRegisterCircleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CanNotRegisterCircleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}