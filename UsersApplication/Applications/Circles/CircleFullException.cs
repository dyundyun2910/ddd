﻿using System;
using System.Runtime.Serialization;
using UsersApplication.Domain.Models.Circles;

namespace UsersApplication.Applications.Circles
{
    [Serializable]
    public class CircleFullException : Exception
    {
        private CircleId id;

        public CircleFullException()
        {
        }

        public CircleFullException(CircleId id)
        {
            this.id = id;
        }

        public CircleFullException(string message) : base(message)
        {
        }

        public CircleFullException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CircleFullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}