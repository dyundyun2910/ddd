﻿namespace UsersApplication.Applications.Users
{
    public interface IUserRegisterService
    {
        void Handle(UserRegisterCommand command);
    }
}
