﻿using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Applications.Users
{
    public class UserGetInfoService : IUserGetInfoService
    {
        private readonly IUserRepository userRepository;

        public UserGetInfoService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public UserData Handle(string userId)
        {
            var targetId = new UserId(userId);
            var user = userRepository.Find(targetId);

            if (user == null)
            {
                return null;
            }

            return new UserData(user);
        }

    }
}
