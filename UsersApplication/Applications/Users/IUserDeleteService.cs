﻿namespace UsersApplication.Applications.Users
{
    public interface IUserDeleteService
    {
        void Handle(UserDeleteCommand command);
    }
}
