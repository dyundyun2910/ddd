﻿using System.Transactions;
using UsersApplication.Domain.Models.Users;
using UsersApplication.Domain.Services.Users;

namespace UsersApplication.Applications.Users
{
    public class UserRegisterService : IUserRegisterService
    {
        private readonly IUserFactory userFactory;
        private readonly IUserRepository userRepository;
        private readonly UserService userService;

        public UserRegisterService(IUserFactory userFactory, IUserRepository userRepository, UserService userService)
        {
            this.userFactory = userFactory;
            this.userRepository = userRepository;
            this.userService = userService;
        }

        public void Handle(UserRegisterCommand command)
        {
            using (var transaction = new TransactionScope())
            {
                var userName = new UserName(command.Name);
                var user = userFactory.Create(userName);

                if (userService.Exists(user))
                {
                    throw new CanNotRegisterUserException(user, "ユーザはすでに存在しています。");
                }

                userRepository.Save(user);
                transaction.Complete();
            }
        }
    }
}
