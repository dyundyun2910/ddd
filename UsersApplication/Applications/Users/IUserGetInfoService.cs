﻿namespace UsersApplication.Applications.Users
{
    public interface IUserGetInfoService
    {
        UserData Handle(string userId);
    }
}
