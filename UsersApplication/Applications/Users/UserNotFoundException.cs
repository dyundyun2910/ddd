﻿using System;
using System.Runtime.Serialization;
using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Applications.Users
{
    [Serializable]
    internal class UserNotFoundException : Exception
    {
        private UserId targetId;

        public UserNotFoundException()
        {
        }

        public UserNotFoundException(UserId targetId)
        {
            this.targetId = targetId;
        }

        public UserNotFoundException(string message) : base(message)
        {
        }

        public UserNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}