﻿using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Applications.Users
{
    public class UserDeleteService : IUserDeleteService
    {
        private readonly IUserRepository userRepository;

        public UserDeleteService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public void Handle(UserDeleteCommand command)
        {
            var targetId = new UserId(command.Id);
            var user = userRepository.Find(targetId);

            if (user == null)
            {
                throw new UserNotFoundException(targetId);
            }

            userRepository.Delete(user);

        }
    }
}
