﻿using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Applications.Users
{
    public class UserData
    {
        public UserData(User source)
        {
            Id = source.Id.Value;
            Name = source.Name.Value;
            Mail = source.Mail.Address;
        }

        public string Id { get; }
        public string Name { get; }
        public string Mail { get; }
    }
}
