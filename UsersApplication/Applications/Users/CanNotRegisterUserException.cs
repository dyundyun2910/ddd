﻿using System;
using System.Runtime.Serialization;
using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Applications.Users
{
    [Serializable]
    public class CanNotRegisterUserException : Exception
    {
        private User user;
        private string v;

        public CanNotRegisterUserException()
        {
        }

        public CanNotRegisterUserException(string message) : base(message)
        {
        }

        public CanNotRegisterUserException(User user, string v)
        {
            this.user = user;
            this.v = v;
        }

        public CanNotRegisterUserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CanNotRegisterUserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}