﻿namespace UsersApplication.Applications.Users
{
    public interface IUserUpdateInfoService
    {
        void Handle(UserUpdateCommand command);
    }
}
