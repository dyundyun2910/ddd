﻿using System.Net.Mail;
using UsersApplication.Domain.Models.Users;
using UsersApplication.Domain.Services.Users;

namespace UsersApplication.Applications.Users
{
    public class UserUpdateInfoService : IUserUpdateInfoService
    {
        private readonly IUserRepository userRepository;
        private readonly UserService userService;

        public UserUpdateInfoService(IUserRepository userRepository, UserService userService)
        {
            this.userRepository = userRepository;
            this.userService = userService;
        }

        public void Handle(UserUpdateCommand command)
        {
            var targetId = new UserId(command.Id);
            var user = userRepository.Find(targetId);
            if (user == null)
            {
                throw new UserNotFoundException(targetId);
            }

            var name = command.Name;
            if (name != null)
            {
                var newUserName = new UserName(name);
                user.ChangeName(newUserName);
                if (userService.Exists(user))
                {
                    throw new CanNotRegisterUserException(user, "ユーザはすでに存在しています。");
                }
            }

            var mailAddress = command.MailAddress;
            if (mailAddress != null)
            {
                var newMailAddresss = new MailAddress(mailAddress);
                user.ChangeMailAddress(newMailAddresss);
            }

            userRepository.Save(user);
        }
    }
}
