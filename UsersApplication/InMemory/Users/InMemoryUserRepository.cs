﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using UsersApplication.Domain.Models.Users;

namespace UsersApplication.InMemory.Users
{
    public class InMemoryUserRepository : IUserRepository
    {
        public Dictionary<UserId, User> Store { get; } = new Dictionary<UserId, User>();

        public User Find(UserName userName)
        {
            var target = Store.Values.FirstOrDefault(user => userName.Equals(user.Name));

            if (target != null)
            {
                return Clone(target);
            }
            else
            {
                return null;
            }
        }

        public User Find(UserId userId)
        {
            var target = Store.Values.FirstOrDefault(user => userId.Equals(user.Id));

            if (target != null)
            {
                return Clone(target);
            }
            else
            {
                return null;
            }
        }

        public User Find(MailAddress mailAddress)
        {
            var target = Store.Values.FirstOrDefault(user => mailAddress.Equals(user.Mail));

            if (target != null)
            {
                return Clone(target);
            }
            else
            {
                return null;
            }
        }

        public void Delete(User user)
        {
            if (user == null) return;
            Store.Remove(user.Id);
        }

        public void Save(User user)
        {
            Store[user.Id] = Clone(user);
        }

        private User Clone(User user)
        {
            return new User(user.Id, user.Name, user.Mail);
        }
    }
}
