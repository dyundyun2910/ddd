﻿using UsersApplication.Domain.Models.Users;

namespace UsersApplication.InMemory.Users
{
    public class InMemoryUserFactory : IUserFactory
    {
        private int currentId = 0;
        public User Create(UserName name)
        {
            currentId++;
            return new User(new UserId(currentId.ToString()), name);
        }
    }
}
