﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;
using UsersApplication.Applications.Users;
using UsersApplication.Domain.Models.Users;
using UsersApplication.Domain.Services.Users;
using UsersApplication.InMemory.Users;

namespace UsersApplication
{
    public partial class Form1 : Form
    {
        private ServiceProvider serviceProvider;

        public Form1()
        {
            InitializeComponent();
            StartUp();
        }

        private void StartUp()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IUserRepository, InMemoryUserRepository>();
            serviceCollection.AddTransient<UserService>();
            serviceCollection.AddTransient<UserRegisterService>();

            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private void BtnCreateUser_Click(object sender, EventArgs e)
        {
            var userRegisterService = serviceProvider.GetService<UserRegisterService>();
            var command = new UserRegisterCommand(textBoxUserName.Text);

            try
            {
                userRegisterService.Handle(command);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

                return;
            }

            MessageBox.Show(this, "ユーザを登録しました。", "メッセージ",
                MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
    }
}
