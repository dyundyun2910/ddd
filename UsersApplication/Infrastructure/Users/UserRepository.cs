﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Mail;
using UsersApplication.Domain.Models.Users;

namespace UsersApplication.Infrastructure.Users
{
    public class UserRepository : IUserRepository
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        public void Save(User user)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"
MERGE INTO users
  USING (
    SELECT @id AS id, @name AS name, @mail AS mail
  ) as data
  ON users.id = data.id
  WHEN MATCHED THEN
    UPDATE  SET name = data.name, mail = data.mail
  WHEN NOT MATCHED THEN
    INSERT (id, name, mail)
    VALUE (data.id, data.name, data.mail);
";
                command.Parameters.Add(new SqlParameter("@id", user.Id.Value));
                command.Parameters.Add(new SqlParameter("@name", user.Name.Value));
                command.Parameters.Add(new SqlParameter("@mail", user.Mail));

                command.ExecuteNonQuery();
            }
        }

        public User Find(UserName userName)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = "SELECT * FROM users WHERE name = @name";
                command.Parameters.Add(new SqlParameter("@name", userName.Value));
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var id = reader["id"] as string;
                        var name = reader["name"] as string;
                        var mail = reader["mail"] as string;

                        return new User(
                            new UserId(id),
                            new UserName(name),
                            new MailAddress(mail)
                            );
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public User Find(UserId userId)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = "SELECT * FROM users WHERE id = @id";
                command.Parameters.Add(new SqlParameter("@id", userId.Value));
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var id = reader["id"] as string;
                        var name = reader["name"] as string;
                        var mail = reader["mail"] as string;

                        return new User(
                            new UserId(id),
                            new UserName(name),
                            new MailAddress(mail)
                            );
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public void Delete(User user)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = "DELETE FROM users WEHRE id = @id";
                command.Parameters.Add(new SqlParameter("@id", user.Id.Value));

                command.ExecuteNonQuery();
            }
        }
    }
}
